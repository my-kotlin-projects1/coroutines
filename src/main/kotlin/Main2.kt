import kotlinx.coroutines.*
import java.math.BigInteger
import java.util.*
import kotlin.concurrent.thread
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlin.system.measureTimeMillis

val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
    println("error from handler ${throwable.message}")
}

val scope = CoroutineScope(Dispatchers.Default)

fun main() {
    runBlocking {
        launch {
            try {
                println("Start runBlocking...")
                suspendMagicNumber()
                println("Finish runBlocking.")
            } catch (t: Throwable) {
            }
        }
        println("Start working...")
    }
}

suspend fun doSomeWork() {
    println("Start work...")
    delay(3000)
    println("Stop work.")
}

suspend fun suspendMagicNumber() {
    suspendCoroutine<BigInteger> { continuation ->
        magicNumber(object : CallBack {
            override fun onSuccess(value: BigInteger) {
                println("number $value")
                continuation.resume(value)
            }

            override fun onFailure(error: Throwable) {
                println("error ${error.message} ")
                continuation.resumeWithException(error)
            }
        })
    }
}

fun magicNumber(callBack: CallBack) {
    thread {
        println("Calculation started: ${Thread.currentThread().name}")
        val number: BigInteger
        val time = measureTimeMillis {
            number = BigInteger.probablePrime(4000, Random())
        }
        if (time > 2000) {
            callBack.onFailure(Throwable("Calculation takes too long"))
        } else {
            callBack.onSuccess(number)
        }
    }
}

interface CallBack {
    fun onSuccess(value: BigInteger)
    fun onFailure(error: Throwable)
}